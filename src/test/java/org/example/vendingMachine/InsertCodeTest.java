package org.example.vendingMachine;

import org.junit.Test;

import com.sda.geotrif.vmo.VendingMachineImplementation;

import junit.framework.TestCase;

public class InsertCodeTest extends TestCase {

	@Test
	void test() {
		VendingMachineImplementation vendingMachineTest = new VendingMachineImplementation();
		int output = vendingMachineTest.insertCode();
		assertEquals(1, output);
	}

}
