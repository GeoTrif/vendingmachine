package org.example.vendingMachine;

import org.junit.Test;

import com.sda.geotrif.vmo.VendingMachineImplementation;

import junit.framework.TestCase;

public class GetRefundTest extends TestCase {

	@Test
	void test() {
		VendingMachineImplementation vendingMachineTest = new VendingMachineImplementation();
		double output = vendingMachineTest.getRefund();
		assertEquals(0, output);
	}

}
