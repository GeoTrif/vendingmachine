package org.example.vendingMachine;

import org.junit.Test;

import com.sda.geotrif.vmo.VendingMachineImplementation;

import junit.framework.TestCase;

public class InsertCoinsTest extends TestCase {

	@Test
	void coinsInsertedTest() {
		VendingMachineImplementation vendingMachineTest = new VendingMachineImplementation();
		double output = vendingMachineTest.insertCoins();
		assertEquals(100, output);
	}
}
