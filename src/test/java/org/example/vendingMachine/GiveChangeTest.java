package org.example.vendingMachine;

import org.junit.Test;

import com.sda.geotrif.vmo.VendingMachineImplementation;

import junit.framework.TestCase;

public class GiveChangeTest extends TestCase {

	@Test
	void test() {
		VendingMachineImplementation vendingMachineTest = new VendingMachineImplementation();
		double output = vendingMachineTest.giveChange();
		assertEquals(0, output);

	}
}
