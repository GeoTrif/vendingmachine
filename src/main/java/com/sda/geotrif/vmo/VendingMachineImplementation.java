package com.sda.geotrif.vmo;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

import com.sda.geotrif.model.Coins;
import com.sda.geotrif.model.ItemList;
import com.sda.geotrif.util.NotFullPaidException;
import com.sda.geotrif.util.NotSufficientChangeException;
import com.sda.geotrif.util.ScannerUtil;

/**
 * Operation class used to make actions on the vending machine.
 * 
 * @author GeoTrif
 *
 */
public class VendingMachineImplementation implements IVendingMachine {
	private double coinsInserted;
	private int code;
	private double price;
	private double change;
	private double refund;
	private ItemList itemList = new ItemList();
	private ScannerUtil scanner = new ScannerUtil();

	public VendingMachineImplementation() {

	}

	public VendingMachineImplementation(double coinsInserted, int code) {
		this.coinsInserted = coinsInserted;
		this.code = code;
	}

	public void setCoinsInserted(double coins) {
		this.coinsInserted = coins;
	}

	public double getCoinsInserted() {
		return coinsInserted;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	/**
	 * Return the amount of money introduced.
	 * 
	 * @return coinsInserted which is is the sum of physical coins introduced in the
	 *         machine.
	 */
	public double insertCoins() {
		System.out.println("Insert coins:");
		this.coinsInserted = scanner.doubleScanner();
		// String coinType = scanner.stringScanner();
		//
		// while (!(coinType.equals("done"))) {
		// this.coinsInserted += Coins.convertCoinToValue(coinType).ordinal();
		// }

		return this.coinsInserted;
	}

	/**
	 * Return the code that process the order.
	 * 
	 * @return code which is the code that indicates the position of the stack on
	 *         the vending machine.
	 */
	public int insertCode() {
		System.out.println("Insert Code:");
		this.code = scanner.integerScanner();
		if (code >= 1 && code <= 10) {
			System.out.println(code);
			return this.code;

		} else {
			System.out.println("Enter a code from 1 to 10.");
		}

		System.out.println(code);

		return 0;

	}

	/**
	 * Resets the refund to 0 in order to prevent multiple refunds.
	 */
	public void refundCheck() {
		this.refund = 0;
	}

	/**
	 * Returns the change after the user placed the order.
	 * 
	 * @return refund if the amount of money introduced is greater that the price of
	 *         the object.
	 */
	public double getRefund() {
		try {
			this.refund = this.coinsInserted;
			if (this.coinsInserted == this.refund) {
				return this.refund;
			} else {
				throw new NotSufficientChangeException(
						"Not sufficient change.Please contact the owner of the vending machine.");
			}
		} catch (NotSufficientChangeException e) {
			System.out.println("Not sufficient change.Please contact the owner of the vending machine.");
		}
		return 0;
	}

	/**
	 * Choice selector for item order.
	 */
	public void cancelRequest() {
		System.out.println("Do you want to purchace the selected item?Yes/No");
		String choice = scanner.stringScanner();
		switch (choice) {
		case "yes":
			giveProduct();
			break;

		case "no":
			System.out.println("You cancelled the order.");
			System.out.println("Do you want to make another order?Yes/No");
			String option = scanner.stringScanner();

			if (option.equalsIgnoreCase("no")) {
				System.out.println("Change:" + giveChange());
				System.exit(0);
			}

			break;

		default:
			System.out.println("Invalid command.");
		}
	}

	/**
	 * Returns the difference between the amount of money introduced and the price
	 * of the product.
	 * 
	 * @return change
	 */
	public double giveChange() {
		this.change = coinsInserted - price;
		return change;
	}

	/**
	 * Changes the current change in order to process another order.
	 * 
	 * @return change
	 */
	public double changeUpdate() {
		this.change -= this.change;
		return change;
	}

	/**
	 * Returns the product from the vending storage for the code introduces.
	 * 
	 * @see the product that the user selected via the code.
	 */
	public void giveProduct() {

		itemList.addItemList();

		switch (this.code) {
		case 1:

			if (this.coinsInserted >= itemList.itemMap.get(1)) {
				System.out.println("You have ordered: Coca Cola : " + itemList.itemMap.get(1));
				this.coinsInserted = this.coinsInserted - itemList.itemMap.get(1);
			} else {
				throw new NotFullPaidException("Insufficient funds.");
			}
			break;

		case 2:
			if (this.coinsInserted >= itemList.itemMap.get(2)) {
				System.out.println("You have ordered: Lays with Salt : " + itemList.itemMap.get(2));
				this.coinsInserted = this.coinsInserted - itemList.itemMap.get(2);
			} else {
				throw new NotFullPaidException("Insufficient funds.");
			}
			break;

		case 3:
			if (this.coinsInserted >= itemList.itemMap.get(3)) {
				System.out.println("You have ordered: Snickers : " + itemList.itemMap.get(3));
				this.coinsInserted = this.coinsInserted - itemList.itemMap.get(3);
			} else {
				throw new NotFullPaidException("Insufficient funds.");
			}
			break;

		case 4:
			if (this.coinsInserted >= itemList.itemMap.get(4)) {
				System.out.println("You have ordered: Apa Borsec Plata : " + itemList.itemMap.get(4));
				this.coinsInserted = this.coinsInserted - itemList.itemMap.get(4);
			} else {
				throw new NotFullPaidException("Insufficient funds.");
			}
			break;

		case 5:
			if (this.coinsInserted >= itemList.itemMap.get(5)) {
				System.out.println("You have ordered: Apa Borsec Minerala : " + itemList.itemMap.get(5));
				this.coinsInserted = this.coinsInserted - itemList.itemMap.get(5);
			} else {
				throw new NotFullPaidException("Insufficient funds.");
			}
			break;

		case 6:
			if (this.coinsInserted >= itemList.itemMap.get(6)) {
				System.out.println("You have ordered: Kit Kat : " + itemList.itemMap.get(6));
				this.coinsInserted = this.coinsInserted - itemList.itemMap.get(6);
			} else {
				throw new NotFullPaidException("Insufficient funds.");
			}
			break;

		case 7:
			if (this.coinsInserted >= itemList.itemMap.get(7)) {
				System.out.println("You have ordered: Saratele Boromir : " + itemList.itemMap.get(7));
				this.coinsInserted = this.coinsInserted - itemList.itemMap.get(7);
			} else {
				throw new NotFullPaidException("Insufficient funds.");
			}
			break;

		case 8:
			if (this.coinsInserted >= itemList.itemMap.get(8)) {
				System.out.println("You have ordered: Snacks Star : " + itemList.itemMap.get(8));
				this.coinsInserted = this.coinsInserted - itemList.itemMap.get(8);
			} else {
				throw new NotFullPaidException("Insufficient funds.");
			}
			break;

		case 9:
			if (this.coinsInserted >= itemList.itemMap.get(9)) {
				System.out.println("You have ordered: Kinder Chocolate : " + itemList.itemMap.get(9));
				this.coinsInserted = this.coinsInserted - itemList.itemMap.get(9);
			} else {
				throw new NotFullPaidException("Insufficient funds.");
			}
			break;

		case 10:
			if (this.coinsInserted >= itemList.itemMap.get(10)) {
				System.out.println("You have ordered: Fanta Lemon : " + itemList.itemMap.get(10));
				this.coinsInserted = this.coinsInserted - itemList.itemMap.get(10);
			} else {
				throw new NotFullPaidException("Insufficient funds.");
			}
			break;

		default:
			System.out.println("Please enter a code from 1 to 10.");
		}
	}
}