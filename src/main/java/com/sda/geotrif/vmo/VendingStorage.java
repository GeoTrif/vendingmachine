package com.sda.geotrif.vmo;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.sda.geotrif.model.ItemList;

/**
 * Operation class used to make operation on the vending machine storage.
 * 
 * @author GeoTrif
 *
 */
public class VendingStorage {

	private static final int STORAGE_SIZE = 20;
	private boolean available;
	private int numberOfProducts;
	private ItemList itemList = new ItemList();

	/**
	 * Sets the number of products that can be stored per row.
	 * 
	 * @param numberOfProducts
	 */
	public void setNumberOfProducts(int numberOfProducts) {
		if (numberOfProducts <= STORAGE_SIZE) {
			this.numberOfProducts = numberOfProducts;
		} else {
			System.out.println("The number of products is over the limit.");
			this.numberOfProducts = STORAGE_SIZE;
		}

	}

	/**
	 * Returns the number of products that can be stored per row.
	 * 
	 * @return numberOfProducts
	 */
	public int getNumberOfProducts() {
		return numberOfProducts;
	}

	/**
	 * Indicates if the row selected has products.
	 * 
	 * @return available
	 */
	public boolean isAvailable() {
		if (this.numberOfProducts > 0) {
			available = true;
		} else {
			available = false;
		}

		return available;
	}

}