package com.sda.geotrif.vmo;

/**
 * Interface used to model the vending machine.
 * 
 * @author GeoTrif
 *
 */
public interface IVendingMachine {

	public double insertCoins();

	public int insertCode();

	public void refundCheck();

	public double getRefund();

	public void cancelRequest();

	public double giveChange();

	public double changeUpdate();

	public void giveProduct();

}