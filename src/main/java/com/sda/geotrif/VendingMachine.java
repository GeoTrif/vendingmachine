package com.sda.geotrif;

import java.util.Scanner;

import com.sda.geotrif.controller.VendingMachineController;
import com.sda.geotrif.model.ItemList;
import com.sda.geotrif.util.Menu;
import com.sda.geotrif.util.NotFullPaidException;
import com.sda.geotrif.util.NotSufficientChangeException;
import com.sda.geotrif.vmo.VendingMachineImplementation;
import com.sda.geotrif.vmo.VendingStorage;

/**
 * Driver class used to start the application.
 * 
 * @author GeoTrif
 *
 */
public class VendingMachine {

	private static VendingMachineController controller = new VendingMachineController();

	/**
	 * Driver method.
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		controller.menuFunctionality();
	}
}