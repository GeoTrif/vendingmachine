package com.sda.geotrif.util;

import java.util.Scanner;

/**
 * Utilitary class used for keyboard inputs.
 * 
 * @author GeoTrif
 *
 */
public class ScannerUtil {

	private Scanner input = new Scanner(System.in);

	public int integerScanner() {
		return input.nextInt();
	}

	public String stringScanner() {
		return input.next();
	}

	public double doubleScanner() {
		return input.nextDouble();
	}

}