package com.sda.geotrif.util;

/**
 * Utilitary class used to declare a particular exception.
 * 
 * @author GeoTrif
 *
 */
public class NotSufficientChangeException extends RuntimeException {

	public String msg;

	public NotSufficientChangeException() {

	}

	public NotSufficientChangeException(String msg) {
		this.msg = msg;
	}
}