package com.sda.geotrif.util;

/**
 * Utilitary class used to declare a particular exception.
 * 
 * @author GeoTrif
 *
 */
public class SoldOutException {

	public String msg;

	public SoldOutException() {

	}

	public SoldOutException(String msg) {
		this.msg = msg;
	}
}