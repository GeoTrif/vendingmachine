package com.sda.geotrif.util;

/**
 * Utilitary class used to declare a particular exception.
 * 
 * @author GeoTrif
 *
 */
public class NotFullPaidException extends RuntimeException {

	public String msg;

	public NotFullPaidException() {

	}

	public NotFullPaidException(String msg) {
		this.msg = msg;
	}

}