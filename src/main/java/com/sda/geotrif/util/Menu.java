package com.sda.geotrif.util;

/**
 * Utilitary class for printing menus.
 * 
 * @author GeoTrif
 */
public class Menu {

	/**
	 * Show the vending machine menu.
	 */
	public void printMenu() {
		System.out.println("Vending Machine Menu");
		System.out.println("Press");
		System.out.println("\t 1 - Show products.");
		System.out.println("\t 2 - Insert coins and select product.");
		System.out.println("\t 3 - Show menu");
		System.out.println("\t 4 - Quit Menu");
	}

	public void printProducts() {
		System.out.println("Product list:");
		System.out.println("\t 1 - Coca Cola -> 35");
		System.out.println("\t 2 - Lays With Salt -> 45");
		System.out.println("\t 3 - Snickers -> 20");
		System.out.println("\t 4 - Borsec Plata -> 20");
		System.out.println("\t 5 - Borsec Minerala -> 25");
		System.out.println("\t 6 - Kit Kat -> 20");
		System.out.println("\t 7 - Saratele -> 30");
		System.out.println("\t 8 - Snacks Star -> 25");
		System.out.println("\t 9 - Kinder Chocolate -> 45");
		System.out.println("\t 10 - Fanta Lemon -> 35");

	}
}