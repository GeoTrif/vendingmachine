package com.sda.geotrif.model;

import java.util.HashMap;
import java.util.Map;

/**
 * Model class used to make the item list.
 * 
 * @author GeoTrif
 *
 */
public class ItemList {
	public Item cocaCola = new Item(1, 35);
	public Item laysWithSalt = new Item(2, 45);
	public Item snickers = new Item(3, 20);
	public Item borsecPlata = new Item(4, 20);
	public Item borsecMinerala = new Item(5, 25);
	public Item kitKat = new Item(6, 20);
	public Item saratele = new Item(7, 30);
	public Item snacks = new Item(8, 25);
	public Item kinder = new Item(9, 45);
	public Item fanta = new Item(10, 35);

	public Map<Integer, Double> itemMap = new HashMap<>();

	/**
	 * Adds Item objects in the map.
	 */
	public void addItemList() {
		itemMap.put(cocaCola.getCode(), cocaCola.getPrice());
		itemMap.put(laysWithSalt.getCode(), laysWithSalt.getPrice());
		itemMap.put(snickers.getCode(), snickers.getPrice());
		itemMap.put(borsecPlata.getCode(), borsecPlata.getPrice());
		itemMap.put(borsecMinerala.getCode(), borsecMinerala.getPrice());
		itemMap.put(kitKat.getCode(), kitKat.getPrice());
		itemMap.put(saratele.getCode(), saratele.getPrice());
		itemMap.put(snacks.getCode(), snacks.getPrice());
		itemMap.put(kinder.getCode(), kinder.getPrice());
		itemMap.put(fanta.getCode(), fanta.getPrice());
	}

}