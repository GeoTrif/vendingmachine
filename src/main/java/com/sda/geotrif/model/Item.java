package com.sda.geotrif.model;

/**
 * Model class used to make new items.
 * 
 * @author GeoTrif
 *
 */
public class Item {

	private String name;
	private double price;
	private int code;

	public Item(String name, double price) {
		this.name = name;
		this.price = price;
	}

	public Item(int code, double price) {
		this.code = code;
		this.price = price;
	}

	public Item(double price) {
		this.price = price;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public double getPrice() {
		return price;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public int getCode() {
		return code;
	}

	@Override
	public String toString() {
		return "Item [name=" + name + ", price=" + price + ", code=" + code + "]";
	}

}