package com.sda.geotrif.model;

/**
 * Enum used to catalogue the coins introduced in the vending machine.
 * 
 * @author GeoTrif
 *
 */
public enum Coins {

	PENNY(1), NICKEL(5), DIME(10), QUARTER(25), DONE(0);

	private int coinType;

	private Coins(int coinType) {
		this.coinType = coinType;
	}

	public static Coins convertCoinToValue(String type) {

		switch (type) {
		case "penny":
			return PENNY;

		case "nickel":
			return NICKEL;

		case "dime":
			return DIME;

		case "quarter":
			return QUARTER;

		default:
			System.out.println("Please enter another choice or done to finish.");
			return DONE;
		}
	}

}