package com.sda.geotrif.controller;

import com.sda.geotrif.util.Menu;
import com.sda.geotrif.util.NotFullPaidException;
import com.sda.geotrif.util.NotSufficientChangeException;
import com.sda.geotrif.util.ScannerUtil;
import com.sda.geotrif.vmo.VendingMachineImplementation;

/**
 * Controller class used to make operations on the vending machine.
 * 
 * @author GeoTrif
 * 
 * 
 */
public class VendingMachineController {

	private VendingMachineImplementation vendingMachine = new VendingMachineImplementation();
	private boolean quit = false;
	private Menu menu = new Menu();
	private ScannerUtil scanner = new ScannerUtil();

	public void menuFunctionality() {
		menu.printMenu();
		int choice = 0;

		while (!quit) {
			System.out.print("Enter your choice:");
			choice = scanner.integerScanner();
			choiceSelection(choice);
		}
	}

	/**
	 * Used to navigate the menu.
	 * 
	 * @param number
	 *            which will be the selector for the menu.
	 */
	public void choiceSelection(int number) {

		switch (number) {
		case 1:
			menu.printProducts();
			break;

		case 2: // Accept Coins and select product;return product and remaining change.
			try {
				vendingMachine.insertCoins();
				while (vendingMachine.getCoinsInserted() > 0) {
					vendingMachine.insertCode();
					vendingMachine.cancelRequest();
				}

			} catch (NotFullPaidException e) {
				System.out.println("Insufficient funds.");
			}

			try {
				vendingMachine.getRefund();
				System.out.println("Change = " + vendingMachine.getRefund());
			} catch (NotSufficientChangeException e) {
				System.out.println("Not sufficient change.Please contact the owner of the vending machine.");
			}

			break;

		case 3:
			menu.printMenu();
			break;

		case 4:
			System.out.println("Goodbye!");
			quit = true;
			break;

		default:
			System.out.println("Please press 3 to print menu and select another choice.");

		}
	}
}