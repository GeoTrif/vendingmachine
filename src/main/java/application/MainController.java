package application;

import com.sda.geotrif.vmo.VendingMachineImplementation;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.text.Text;

/**
 * JavaFx Controller class used to make operations on the user interface.
 * 
 * @author GeoTrif
 *
 */
public class MainController {
	@FXML
	private TextField coinsInserted;
	@FXML
	private Label codeInserted;
	@FXML
	private Label order;
	@FXML
	private Button numericButtons;
	@FXML
	private Button orderButton;

	@FXML
	private void codeHandler(ActionEvent e) {
		if (e.getSource() == orderButton) {
			coinsInserted.setText("");
			order.setText("You ordered : ");
			coinsInserted.requestFocus();
			return;
		}

	}

}